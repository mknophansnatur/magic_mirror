#!/bin/bash
DEV="pi@192.168.110.79"
URL2="pi@192.168.110.78"
URL3="pi@192.168.110.76"
URL4="pi@192.168.110.77"
URL1="pi@192.168.110.75"
URL="undefined"
sleeptime=15

echo restarting $URL1
#ssh $URL1 "pm2 restart mm"
echo waiting $sleeptime seconds
sleep $sleeptime
echo restarting $URL2
ssh $URL2 "pm2 restart mm"
echo waiting $sleeptime seconds
sleep $sleeptime
echo restarting $URL3
ssh $URL3 "pm2 restart mm"
echo waiting $sleeptime seconds
sleep $sleeptime
echo restarting $URL4
ssh $URL4 "pm2 restart mm"
echo done!

