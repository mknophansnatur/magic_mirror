#!/bin/bash
DEV="pi@192.168.110.79"
URL2="pi@192.168.110.78"
URL3="pi@192.168.110.76"
URL4="pi@192.168.110.77"
#URL1="pi@192.168.110.75"
URL="undefined"

echo rsync to $URL1
sudo rsync -avz  modules/ $URL1:~/MagicMirror/modules
sudo rsync -avz  css/ $URL1:~/MagicMirror/css
sudo rsync -avz  config/ $URL1:~/MagicMirror/config

echo rsync to $URL2
sudo rsync -avz  modules/ $URL2:~/MagicMirror/modules
sudo rsync -avz  css/ $URL2:~/MagicMirror/css
sudo rsync -avz  config/ $URL2:~/MagicMirror/config

echo rsync to $URL3
sudo rsync -avz  modules/ $URL3:~/MagicMirror/modules
sudo rsync -avz  css/ $URL3:~/MagicMirror/css
sudo rsync -avz  config/ $URL3:~/MagicMirror/config

echo rsync to $URL4
sudo rsync -avz  modules/ $URL4:~/MagicMirror/modules
sudo rsync -avz  css/ $URL4:~/MagicMirror/css
sudo rsync -avz  config/ $URL4:~/MagicMirror/config
