Module.register("profit_graph",{
	// Default module config.
	defaults: {
        text: "Profit Graph",
        chartConfig: {
            type: 'scatter',
            data: {
                labels: ["Label"],
                datasets: [{
                    label: 'Labels pro Stunde',
                    data: [{
                        x: 0,
                        y: 0,
                    },
                    {
                        x: 1,
                        y: 2
                    }
                    ],
                   fill: false,
                   borderColor: 'rgba(112, 195, 237)',
                   showLine: true,
                }]
            },
            options: {
                tension: 0,
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        ticks: {
                            stepSize: 1
                        },
                        scaleLabel: {
                            labelString: "Stunden",
                            display: true
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            labelString: "Labels",
                            display: true
                        }
                    }]
                },
            }
        },
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
            this.file("/node_modules/chart.js/dist/Chart.bundle.min.js"),
            this.file("public/profit_graph_helper.js")
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'profit_graph--wrapper');

         //console.log(JSON.stringify(this.config.chartConfig));
         const wrapperEl = wrapper;
         var e =document.createElement("div");    // add div to give size to chart area
         //e.setAttribute("style", "position: relative; display: inline-block;");  // position already set
         e.style.width = this.config.width+"px";   // set right style attribute with size in pixels
         e.style.height = this.config.height+"px";   // set right style attribute with size in pixels     
         wrapperEl.appendChild(e);
         // Create chart canvas
         const chartEl = document.createElement("canvas");
         // Append chart
         e.appendChild(chartEl);
         // Init chart.js
         this.chart = new Chart(chartEl, this.config.chartConfig);
 
         return wrapperEl;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';

            $('.profit_graph--wrapper').append(content);
        }
        if(notification == 'LINEGRAPH') {
            console.log("IN LINEGRAPH");
            console.log(payload.message);
            console.log(this.chart.data.datasets[0].data.length);
            /* for(let i = 0; i < this.chart.data.datasets[0].data.length; i++) {
                this.chart.data.datasets[0].data[i].pop();
            } */
            this.chart.data.datasets[0].data.pop();
            this.chart.update();
            var forGraph = [];
            var data = payload.message.value;
            /* for (let index = 0; index < data.length; index++) {
                const element = data[index];
                //console.log(element.timestamp);
                this.chart.data.datasets[0].data[index].pop();
                
            } */
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                //console.log(element.timestamp);
                var t = new Date(element.timestamp);
                //console.log(forGraph);
                var jsonData = {
                    x: t.getHours(),
                    y: element.data
                }
                forGraph.push(jsonData);
                //console.log(t);
                this.chart.data.datasets[0].data[index] = jsonData;
                
            }
            //console.log(forGraph);
            /* var xAxe = [];
            var theData = this.chart.data.datasets[0].data;
            var value = [parseInt(payload.message.value)];

            var forGraph = {
                x: theData.length,
                y: value
            } */

            /* console.log("from the last: " );
            console.log(theData);
            console.log(forGraph); */
            //console.log(theData.length);
            console.log(this.chart.data.datasets[0].data);
            //this.chart.data.datasets[0].data[0] = forGraph;
            //console.log(this.chart.data.datasets[0].data[0]);
            this.chart.update();
            /* var theData = [{
                x : 
            }] */
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in profit_graph");
        this.config = Object.assign({}, this.defaults, this.config);
    }
});