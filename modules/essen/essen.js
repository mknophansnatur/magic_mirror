//var day = false;
Module.register("essen",{
	// Default module config.
	defaults: {
		text: "Essen"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'essen--wrapper');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="essen--container">';
            content += '</div>';
            $('.essen--wrapper').append(content);
        }
        if(notification == 'FOOD') {
            $('.essen--container').replaceWith('<div class="essen--container"></div>');
            console.log("from essen");
            var moreContent = '';
            moreContent += '<h2 class="essen--headline-2">'+payload.message.day+'</h2>';
            moreContent += '<div class="essen--actual-wrapper">';
            moreContent += '<div class="essen--team-wrapper">';
            if(payload.message.hasOwnProperty('team')) {
                /* moreContent += '<h3 class="essen--headline-3">Team</h3>'; */
                moreContent += '<img src="/modules/essen/public/team.jpg" class="essen--img">';
                moreContent += '<div class="essen--imgandtext-wrapper">';
                moreContent += '<div class="essen--text-wrapper">';
                moreContent += '<p class="essen--text"><span>Mahl: </span>'+payload.message.team.name+'</p>';
                moreContent += '<p class="essen--text"><span>Preis: </span>'+payload.message.team.price+'</p>';
                moreContent += '</div>';
                moreContent += '</div>';
            }
            moreContent += '</div>';

            moreContent += '<div class="essen--wiese-wrapper">';
            if(payload.message.hasOwnProperty('wiese')) {
    /*             moreContent += '<h3 class="essen--headline-3">Wiese</h3>'; */
                moreContent += '<img src="/modules/essen/public/wiese.png" class="essen--img">';
                moreContent += '<div class="essen--imgandtext-wrapper">';
                moreContent += '<div class="essen--text-wrapper">';
    
                for(var i = 0; i < payload.message.wiese.length; i++) {
                    moreContent += '<p class="essen--text"><span>Mahl: </span>'+payload.message.wiese[i].name+'</p>';
                    moreContent += '<p class="essen--text"><span>Preis: </span>'+payload.message.wiese[i].price+'</p>';
                }
                moreContent += '</div>';
                moreContent += '</div>';
    
            }
            moreContent += '</div>';


            console.log(payload.message);
            $('.essen--container').append(moreContent);
            //day = true;
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in status");
    }
});