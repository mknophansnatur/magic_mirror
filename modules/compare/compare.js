Module.register("compare",{
	// Default module config.
	defaults: {
		text: "Compare"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'compare--wrapper');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="status--block-twothird">';
            content += '<h4 class="status--headline-4">Labels pro Stunde und im Vergleich zu letzter Woche</h4>';
            content += '<div class="status--block-helper">';
            content += '<div class="status--block-left">';
            content += '<img class="status--block-graph compare--graph" src="/modules/compare/public/path4572.png">';
            content += '<img class="status--block-indicator compare--indicator" value="5" src="/modules/status/public/fleet-status_2_new.svg">';
            content += '</div>';
            content += '<div class="status--block-center compare--block-center">';
            content += '<p class="status--block-efficiency compare--efficiency">';
            content += '+12';
            content += '</p>';
            content += '<p class="status--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '<div class="compare--block-right">';
            content += '<p class="status--block-effiText">';
            content += 'Label pro Stunde';
            content += '</p>';
            content += '<h3 class="average--headline-3 compare--perHour">';
            content += '+12';
            content += '</h3>';

            content += '</div>';

            content += '</div>';


            content += '</div>';

            
            /* content += '<div class="status--block-right">';
            content += '<div class="status--right-row">';
            content += '<p class="status--text-grey">';
            content += 'Total Fleet';
            content += '</p>';
            content += '<span class="status--text-number">';
            content += '63';
            content += '</span>';
            content += '</div>';
            content += '<div class="status--right-row">';
            content += '<p class="status--text-grey">';
            content += 'On the Move';
            content += '</p>';
            content += '<span class="status--text-number">';
            content += '63';
            content += '</span>';
            content += '</div>';
            content += '<div class="status--right-row">';
            content += '<p class="status--text-grey">';
            content += 'In Maintenance';
            content += '</p>';
            content += '<span class="status--text-number">';
            content += '63';
            content += '</span>';
            content += '</div>';
            content += '</div>'; */
            $('.compare--wrapper').append(content);
        }
        if(notification == 'COMPARE') {
            console.log("from compare");
            if(payload.message.type === "deliveryStatisticsPerHourFromLastWeek") {
                $('.compare--indicator').attr("value", (payload.message.value));
            }
            if(payload.message.type === "deliveryStatisticsPerHourFromToday") {
                var oldVal = parseInt($('.compare--indicator').attr("value"));
                var newVal = parseInt(payload.message.value);
                var result = parseInt((newVal - oldVal));
                $('.compare--perHour').text(newVal);
                if(result > 2) {
                    $('.compare--indicator').css('transform', 'rotate('+270+'deg)');
                    $('.compare--efficiency').text('+'+result.toString().replace(/\./g, ','));
                    console.log("positive");
                }
                else if (result > 0) {
                    $('.compare--indicator').css('transform', 'rotate('+180+'deg)');
                    $('.compare--efficiency').text('+/- 0');
                    console.log("is gleich")
                }
                else {
                    $('.compare--indicator').css('transform', 'rotate('+90+'deg)');
                    $('.compare--efficiency').text(result.toString().replace(/\./g, ','));
                    console.log("negativ");
                }
                console.log(result);
            }
            //console.log(payload.message);
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in status");
    }
});