Module.register("deliveries_by_country",{
	// Default module config.
	defaults: {
        width       : 430,
        height      : 330,
        chartConfig: {
            type: 'pie',
            data: {
                labels: ["Deutschland", "Österreich", "Frankreich", "Luxemburg", "Niederlande", "Belgien", "Großbritannien", "Italien"],
                datasets: [{
                    label: 'Versandstatistik',
                    data: [35, 25, 21, 19, 1,1,1,1],
                    backgroundColor: [
                        'rgba(255, 165, 0)',
                        'rgba(112, 195, 237)',
                        'rgba(95, 95, 95)',
                        'rgba(165, 165, 165)'
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        },
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
            this.file("/node_modules/chart.js/dist/Chart.bundle.min.js")
        ];
    },


    loaded: function() {
        console.log("in loaded");
    },

    start: function() {
        this.config = Object.assign({}, this.defaults, this.config);
		Log.info("Starting module: " + this.name);
	},

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'deliveries_by_country--wrapper');

        // Create wrapper element
        //console.log("*****");
        //console.log(JSON.stringify(this.config.chartConfig));
        const wrapperEl = wrapper;
        var e =document.createElement("div");    // add div to give size to chart area
        e.setAttribute('class', 'deliveries_by_country--helper');
        //e.setAttribute("style", "position: relative; display: inline-block;");  // position already set
        e.style.width = this.config.width+"px";   // set right style attribute with size in pixels
        e.style.height = this.config.height+"px";   // set right style attribute with size in pixels     
        wrapperEl.appendChild(e);
        // Create chart canvas
        const chartEl = document.createElement("canvas");
        // Append chart
        e.appendChild(chartEl);
        // Init chart.js
        this.chart = new Chart(chartEl, this.config.chartConfig);

		return wrapperEl;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="deliveries_by_country--container">';
            content += '<h3 class="deliveries_by_country--headline">'+'Deliveries by Country'+'</h3>';
            content += '<div class="deliveries_by_country--graph-wrapper">';
            /* content += '<div class="deliveries_by_country--chart chart"></div>' */

            content += '<div class="deliveries_by_country--text-wrapper">';
            content += '<div class="deliveries_by_country--row">';
            content += '<span class="deliveries_by_country--rect-orange deliveries_by_country--rect"></span>';
            content += '<p class="deliveries_by_country--row-text">';
            content += 'Germany';
            content += '</p>'
            content += '<p class="deliveries_by_country--row-number">';
            content += '35%';
            content += '</p>'
            //row close
            content += '</div>';

            content += '<div class="deliveries_by_country--row">';
            content += '<span class="deliveries_by_country--rect-lightblue deliveries_by_country--rect"></span>';
            content += '<p class="deliveries_by_country--row-text">';
            content += 'France';
            content += '</p>'
            content += '<p class="deliveries_by_country--row-number">';
            content += '25%';
            content += '</p>'
            //row close
            content += '</div>';

            content += '<div class="deliveries_by_country--row">';
            content += '<span class="deliveries_by_country--rect-darkgrey deliveries_by_country--rect"></span>';
            content += '<p class="deliveries_by_country--row-text">';
            content += 'Switzerland';
            content += '</p>'
            content += '<p class="deliveries_by_country--row-number">';
            content += '21%';
            content += '</p>'
            //row close
            content += '</div>';

            content += '<div class="deliveries_by_country--row">';
            content += '<span class="deliveries_by_country--rect-lightgrey deliveries_by_country--rect"></span>';
            content += '<p class="deliveries_by_country--row-text">';
            content += 'Austria';
            content += '</p>'
            content += '<p class="deliveries_by_country--row-number">';
            content += '19%';
            content += '</p>'
            //row close
            content += '</div>';

            // text wrapper close
            content += '</div>';
            // graph wrapper close
            content += '</div>';
            // container close
            content += '</div>';
            $('.deliveries_by_country--wrapper').append(content);
        }
        if(notification == 'PIE') {
            console.log(payload);
            var countries = payload.message.value;
            var theData = [];
            var labels = [];
            for (let index = 0; index < 8; index++) {
                const count = countries[index].Count;
                const label = countries[index].Country;
                //console.log(date);
                theData.push(count);
                labels.push(label);
            }
            this.chart.data.datasets[0].data = theData;
            //console.log(theData);
            //console.log(labels);
            this.chart.update();
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
});