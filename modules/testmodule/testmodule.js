
Module.register("testmodule",{
	// Default module config.
	defaults: {
		text: "From Rabbitmq"
    },
    
    getScripts: function() {
		return [
            this.file("node_modules/jquery/dist/jquery.js"),
            this.file("public/testmodule_extra.js")
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        var table = document.createElement("table");
        wrapper.setAttribute('class', 'rabbit--wrapper calendar');
        table.setAttribute('class', 'small'); 
        wrapper.appendChild(table);

		return wrapper;
    },

    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        this.sendSocketNotification('START', {message: 'start connection'});
        //console.log("blup bla");
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var add = '<tr class="normal">';
            add += '<td class="symbol align-right ">';
            add += '<span class="fa fa-fw fa-calendar-check-o"></span>';
            add += '</td>';
            add += '<td class="title bright ">name';
            add += '</td>';
            add += '<td class="time light ">20.02.2020</td></tr>';
            $('.rabbit--wrapper table').append(add);
        }
    },

    socketNotificationReceived: function(notification, payload) {
        console.log(payload);
        if(notification === 'MSG') {
            if(IsJsonString(payload)) {
                console.log("is json!");
            }
            else {
                console.log("no valid json");
                return false;
            }
            var obj = JSON.parse(payload);
            if('beleg' in obj) {
                console.log("yoo");
            }
            else {
                console.log("kein beleg gefunden");
                return false;
            }
            var name = obj['beleg'];
            var date = new Date(obj['timestamp']);
            var year = date.getFullYear();
            var month = date.getMonth()+1;
            var dt = date.getDate();
            var hour = date.getHours();
            var minute = date.getMinutes();
            var seconds = date.getSeconds();

            if (dt < 10) {
                dt = '0' + dt;
            }
            if (month < 10) {
                month = '0' + month;
            } 
            var dateString = hour + ":"+minute+":"+seconds+"  "+dt+"."+month+"."+year;
            console.log(name);aaaaaaaaaa
            console.log(dateString);
            var add = '<tr class="normal">';
            add += '<td class="symbol align-right ">';
            add += '<span class="fa fa-fw fa-calendar-check-o"></span>';
            add += '</td>';
            add += '<td class="title bright ">'+ name;
            add += '</td>';
            add += '<td class="time light ">' + dateString + '</td></tr>';
            //var add = '<div class="calendar rabbit--log"><div class=""><p>Beleg: </p>' + beleg + '</div><div class=""><p>Datum: </p>'+dateString+'</div></div>';
            $('.rabbit--wrapper table').append(add);
        }
        else {
            console.log(payload);
        }
        //console.log(payload);
    }

});


function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}