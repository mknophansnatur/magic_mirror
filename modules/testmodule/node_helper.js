const NodeHelper = require('node_helper');

var amqp = require('amqplib/callback_api');

var fs = require('fs');

var mysql = require('mysql');

var server = 'amqp://user:bitnami@192.168.14.13:5672';

var amqpConn = null;

module.exports = NodeHelper.create({
    start: function() {
        console.log(" helper started... " + this.name);
        this.sendSocketNotification('MSG', 'Hello World');
        var self = this;


        function start() {
            amqp.connect(server + "?heartbeat=60", function(err, conn) {
              if (err) {
                console.error("[AMQP]", err.message);
                return setTimeout(start, 1000);
              }
              conn.on("error", function(err) {
                if (err.message !== "Connection closing") {
                  console.error("[AMQP] conn error", err.message);
                }
              });
              conn.on("close", function() {
                console.error("[AMQP] reconnecting");
                return setTimeout(start, 1000);
              });
          
              console.log("[AMQP] connected");
              amqpConn = conn;
          
              whenConnected();
            });
          }
          
          function whenConnected() {
            startPublisher();
            startWorker();
          }
          
          var pubChannel = null;
          var offlinePubQueue = [];
          function startPublisher() {
            amqpConn.createConfirmChannel(function(err, ch) {
              if (closeOnErr(err)) return;
              ch.on("error", function(err) {
                console.error("[AMQP] channel error", err.message);
              });
              ch.on("close", function() {
                console.log("[AMQP] channel closed");
              });
          
              pubChannel = ch;
              while (true) {
                var m = offlinePubQueue.shift();
                if (!m) break;
                publish(m[0], m[1], m[2]);
              }
            }); 
          }
          
          // method to publish a message, will queue messages internally if the connection is down and resend later
          function publish(exchange, routingKey, content) {
            try {
                pubChannel.publish(exchange, routingKey, content, { persistent: true },
                function(err, ok) {
                if (err) {
                    console.error("[AMQP] publish", err);
                    offlinePubQueue.push([exchange, routingKey, content]);
                    pubChannel.connection.close();
                }
                });
            } catch (e) {
              console.error("[AMQP] publish", e.message);
              offlinePubQueue.push([exchange, routingKey, content]);
            }
          }
          
          // A worker that acks messages only if processed succesfully
          function startWorker() {
            amqpConn.createChannel(function(err, ch) {
              if (closeOnErr(err)) return;
              ch.on("error", function(err) {
                console.error("[AMQP] channel error", err.message);
              });
              ch.on("close", function() {
                console.log("[AMQP] channel closed");
              });
              ch.prefetch(10);
              ch.assertQueue("jobs", { durable: true }, function(err, _ok) {
                if (closeOnErr(err)) return;
                ch.consume("jobs", processMsg, { noAck: true });
                console.log("Worker is started");
              });
          
              function processMsg(msg) {
                var path = '/opt/magic_mirror/modules/testmodule/message.txt';
                work(msg, function(ok) {
                  try {
                    if (ok) {
                      try {
                        if(fs.existsSync(path)) {
                          fs.appendFileSync(path,msg.content.toString());
                        }
                      } catch(err) {
                        console.error(err);
                        fs.writeFile(path, msg.content.toString(), {flag: 'wx'}, function(err) {
                          if(err) throw err;
                          console.log("sollte gesaved sein");
                        });
                      }
                      self.sendSocketNotification('MSG', msg.content.toString());
                        //ch.ack(msg);
                    }
                    else
                      ch.reject(msg, true);
                  } catch (e) {
                    closeOnErr(e);
                  }
                });
              }
            });
          }
          
          function work(msg, cb) {
            console.log("Got msg", msg.content.toString());
            cb(true);
          }
          
          function closeOnErr(err) {
            if (!err) return false;
            console.error("[AMQP] error", err);
            amqpConn.close();
            return true;
          }
          
          setInterval(function() {
            //publish("", "jobs", new Buffer("work work work"));
          }, 10000);
          
          //start();





          var con = mysql.createConnection({
            host: "localhost:3306",
            user: "user",
            password: "password"
          });
          
          con.connect(function(err) {
            if (err) throw err;
            console.log("Connected!");
          });



    },

    socketNotificationReceived: function(notification, payload) {
        console.log("received msg new" + payload);
        var self = this;
        /*
        amqp.connect(server, function(error0, connection) {
            if (error0) {
                throw error0;
            }
            connection.createChannel(function(error1, channel) {
                if (error1) {
                    throw error1;
                }
                var exchange = 'magicmirror';

                channel.assertExchange(exchange, 'fanout', {
                    durable: false
                });

                channel.assertQueue('', {
                    exclusive: true
                }, function(error2, q) {
                    if (error2) {
                        throw error2;
                    }
                    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                    channel.bindQueue(q.queue, exchange, '');

                    channel.consume(q.queue, function(msg) {
                        if(msg.content) {
                            console.log(" [x] %s", msg.content.toString());
                            self.sendSocketNotification('MSG', msg.content.toString());
                        }
                    }, {
                        noAck: true
                    });
                });
            });
        });
        */        

        /*amqp.connect(server, function(error0, connection) {
            if (error0) {
                throw error0;
            }
            connection.createChannel(function(error1, channel) {
                if (error1) {
                    throw error1;
                }
                //var queue = 'Packtisch.CreateLabel';
                var queue = 'magicmirror';

                channel.assertQueue(queue, {
                    durable: true,
                    arguments: {
                        "x-queue-type": "classic"
                    }
                });
                channel.prefetch(1);
                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
                channel.consume(queue, function(msg) {
                    var secs = msg.content.toString().split('.').length - 1;

                    console.log(" [x] Received %s", msg.content.toString());
                    //channel.ack(msg);
                    self.sendSocketNotification('MSG', msg.content.toString());
                    //self.sendSocketNotification('OBJ', msg);
                }, 
                {
                    noAck: true
                });
            });
        });*/
    },

});


/*
{
  "beleg":"2020-570834",
  "timestamp":"2020-03-03T09:36:19"
}

*/