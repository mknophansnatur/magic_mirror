Module.register("vorlauf",{
	// Default module config.
	defaults: {
        text: "Vorlauftemperatur",
        chartConfig: {
            type: 'scatter',
            data: {
                labels: ["Label"],
                datasets: [{
                    label: 'Aktuelle Erzeugung',
                    data: [{
                        x: 0,
                        y: 0,
                    },
                    {
                        x: 1,
                        y: 2
                    }
                    ],
                   fill: false,
                   borderColor: 'rgba(112, 195, 237)',
                   showLine: true,
                }]
            },
            options: {
                tension: 0,
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        ticks: {
                            stepSize: 1
                        },
                        scaleLabel: {
                            labelString: "Stunden",
                            display: true
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            labelString: "kWh",
                            display: true
                        }
                    }]
                },
            }
        },
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
            this.file("/node_modules/chart.js/dist/Chart.bundle.min.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'vorlauf--wrapper');

         //console.log(JSON.stringify(this.config.chartConfig));
         const wrapperEl = wrapper;
         var e =document.createElement("div");    // add div to give size to chart area
         //e.setAttribute("style", "position: relative; display: inline-block;");  // position already set
         e.style.width = 640+"px";   // set right style attribute with size in pixels
         e.style.height = 320+"px";   // set right style attribute with size in pixels     
         wrapperEl.appendChild(e);
         // Create chart canvas
         const chartEl = document.createElement("canvas");
         // Append chart
         e.appendChild(chartEl);
         // Init chart.js
         this.chart = new Chart(chartEl, this.config.chartConfig);
 
         return wrapperEl;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';

            $('.vorlauf--wrapper').append(content);
        }
        if(notification == 'VORLAUF') {
            console.log("IN VORLAUF");
            console.log(payload.message);
            var forGraph = [];
            this.chart.data.datasets[0].data.pop();
            this.chart.update();
            var data = payload.message.value;
            
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                //console.log(element.timestamp);
                var t = new Date(element.timestamp);
                //console.log(forGraph);
                var jsonData = {
                    x: element.hour,
                    y: element.value
                }
                forGraph.push(jsonData);
                //console.log(t);
                this.chart.data.datasets[0].data[index] = jsonData;
                
            } 
            
            console.log(this.chart.data.datasets[0].data);

            this.chart.update();

        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in profit_graph");
        this.config = Object.assign({}, this.defaults, this.config);
    }
});