Module.register("error_handler",{
	// Default module config.
	defaults: {
		text: "Error Handler"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js")
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        //console.log("in getDOM ");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'error_handler--wrapper');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'ERROR') {
            console.log("FROM ERROR HANDLER ************************************");
            console.log(payload.message);
            $(".error_handler--wrapper").append("<div class='alert-window'><p class='alert-text'>"+payload.message+"</p></div>");
        }
    },


    updateDom: function() {
        //console.log("in update dom");

        return true;
    },

    socketNotificationReceived: function(notification, payload) {
        var self = this;
        console.log(notification);
        console.log(payload);
        console.log("***************** in new relic alert");
    },
    
    start: function() {
        var self = this;
        this.sendSocketNotification('START', {message: 'start connection in error_handler'});
        //console.log("in delivery_status");
    }
});


function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}