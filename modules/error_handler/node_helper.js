const NodeHelper = require('node_helper');



module.exports = NodeHelper.create({
    start: function() {
        console.log(" helper started... " + this.name);
        this.sendSocketNotification('MSG', 'Hello World from error handler');
        
      },
      
      socketNotificationReceived: function(notification, payload) {
        console.log("received msg new");
        this.sendSocketNotification('MSG', 'Hello from error handler helper');
    }

});
