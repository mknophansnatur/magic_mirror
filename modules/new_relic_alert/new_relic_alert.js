Module.register("new_relic_alert",{
	// Default module config.
	defaults: {
		text: "New Relic Alert"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js")
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        //console.log("in getDOM ");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'new_relic_alert--wrapper');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
       
    },


    updateDom: function() {
        //console.log("in update dom");

        return true;
    },

    socketNotificationReceived: function(notification, payload) {
        var self = this;
        //console.log("**************************** in socket notification")
        //console.log(notification);
        //console.log(payload);
        //console.log(notification, payload);
        //console.log("***************** in new relic alert");
        if(notification == 'NWRLC') {
            //console.log("From nodehelper, works yuhu");
            var allGucci = true;
            if(payload) {
                //console.log(notification);
                //console.log(payload);
                for(var i = 0; i < payload.length; i++) {
                    const value = payload[i].values.score;
                    //console.log(value);
                    if(value < 0.7 && i == payload.length-1) {
                        if($('.alert-window').length) {

                        }
                        else {
                            allGucci = false;
                            console.log();
                            var counter = 0;
                            //$('.fa.indicator').last().trigger('click');
                            this.sendNotification("PAGE_CHANGED", 4);
                            //$('body').append('<div class="alert-window"><p>APDEX NIEDRIG!!!</p></div>');
                            this.sendNotification("PAUSE_ROTATION");
                            this.sendNotification("ERROR", {message: "APDEX VALUE IS " + value + " from:" + payload[i].from + " to: "+ payload[i].to})
                            $('.fa.fa-circle.indicator').removeClass("resume");
                            console.log("nich alles gucci");
                        }
                    }
                    else {
                        if($('.alert-window').length && allGucci) {
                            console.log("all gucci");
                            if($('.alert-window').hasClass("fire")) {

                            }
                            else {
                                $('.fa.fa-circle.indicator').addClass("resume");
                                $('.alert-window').remove();
                                allGucci = true;
                                console.log("pimmel");
                                this.sendNotification("RESUME_ROTATION");
                            }
                            //location.reload();
                        }
                        if(i == payload.length-1 && allGucci) {
                            $('.fa.fa-circle.indicator').addClass("resume");
                            if($('.alert-window').hasClass("fire")) {
                                console.log("ende");
                            }
                            else {
                                $('.alert-window').remove();
                                allGucci = true;
                                this.sendNotification("RESUME_ROTATION");
                            }
                            //location.reload();
                        }
                    }
                }
            }
            this.sendSocketNotification('NWRLC', {message: 'keep on working'});
        }
    },
    
    start: function() {
        var self = this;
        this.sendSocketNotification('START', {message: 'start connection in new relic module'});
        //console.log("in delivery_status");
    }
});


function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}