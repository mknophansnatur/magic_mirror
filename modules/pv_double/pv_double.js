Module.register("pv_double",{
	// Default module config.
	defaults: {
        text: "PV_double",
        name1: "Name 1",
        class_name1: "aa",
        wrapper_name: "wrapper_name",
        name2: "Name 2",
        class_name2: "aa"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'pv--wrapper pv--wrapperdouble pv--'+this.config.wrapper_name);

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="pv--container pv--container-double '+this.config.class_name1+'">';
            content += '<h4 class="pv--headline-4">'+this.config.name1+'</h4>';
            content += '<img class="pv--block-graph" src="/modules/pv_double/public/blitz.svg">';
            content += '<div class="pv--block">';
            content += '<p class="pv--block-efficiency '+this.config.class_name1+'">';
            content += '6.992 kwh';
            content += '</p>';
            content += '<p class="pv--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';

            content += '<div class="pv--container pv--container-double '+this.config.class_name2+'">';
            content += '<h4 class="pv--headline-4">'+this.config.name2+'</h4>';
            content += '<img class="pv--block-graph" src="/modules/pv_double/public/blitz.svg">';
            content += '<div class="pv--block">';
            content += '<p class="pv--block-efficiency '+this.config.class_name2+'">';
            content += '6.992 kwh';
            content += '</p>';
            content += '<p class="pv--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            
            $('.pv--'+this.config.wrapper_name).append(content);
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in pv");
    }
});