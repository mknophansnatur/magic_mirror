Module.register("average",{
	// Default module config.
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'average--wrapper');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="average--left-wrapper average--container">';
            content += '<img class="average--left-image average--image-graph" src="/modules/average/public/average_1.svg">';
            content += '<div class="average--left-content">';
            content += '<h3 class="average--headline-3 average--label-minute">'+'25min'+'</h3>';
            content += '<p class="status--block-effiText">';
            content += 'Label pro Minute';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            content += '<div class="average--right-wrapper average--container">';

        
            content += '<img class="average--right-image average--image-weight" src="/modules/average/public/average_weight.svg">';
            content += '<div class="average--right-content">';
            content += '<h3 class="average--headline-3 average--label-value">'+'10 tons'+'</h3>';
            content += '<p class="status--block-effiText">';    
            content += 'Labels Heute';
            content += '</p>';
            content += '</div>';

            content += '</div>';
            $('.average--wrapper').append(content);
        }
        if(notification == 'LABELS') {
            console.log(payload);
            $('.average--label-value').text(payload.message.value);
        }
        if(notification == 'MINUTE') {
            $('.average--label-minute').text(payload.message.value)
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in average");
    }
});