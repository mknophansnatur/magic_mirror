Module.register("status_two",{
	// Default module config.
	defaults: {
		text: "Status"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'status--wrapper status--wrapper_two');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="status--block-twothird">';
            content += '<h4 class="status--headline-4">Raumtemperatur</br> Pausenraum</h4>';
            content += '<div class="status--block-helper">';
            content += '<img class="status--block-graph" src="/modules/status/public/fleet-status_new.svg">';
            content += '<img class="status--block-indicator break" src="/modules/status/public/fleet-status_2_new.svg">';
            /* content += '<div class="status--block-left">';
            content += '</div>'; */
            content += '<div class="status--block-center">';
            content += '<p class="status--block-efficiency break">';
            content += '00°C';
            content += '</p>';
            content += '<p class="status--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';

            content += '<div class="status--block-twothird">';
            content += '<h4 class="status--headline-4">Raumtemperatur</br> Buchhaltung</h4>';
            content += '<div class="status--block-helper">';
            content += '<img class="status--block-graph" src="/modules/status/public/fleet-status_new.svg">';
            content += '<img class="status--block-indicator bh" src="/modules/status/public/fleet-status_2_new.svg">';
            /* content += '<div class="status--block-left">';
            content += '</div>'; */
            content += '<div class="status--block-center">';
            content += '<p class="status--block-efficiency bh">';
            content += '00°C';
            content += '</p>';
            content += '<p class="status--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            /* content += '<div class="status--block-right">';
            content += '<div class="status--right-row">';
            content += '<p class="status--text-grey">';
            content += 'Total Fleet';
            content += '</p>';
            content += '<span class="status--text-number">';
            content += '63';
            content += '</span>';
            content += '</div>';
            content += '<div class="status--right-row">';
            content += '<p class="status--text-grey">';
            content += 'On the Move';
            content += '</p>';
            content += '<span class="status--text-number">';
            content += '63';
            content += '</span>';
            content += '</div>';
            content += '<div class="status--right-row">';
            content += '<p class="status--text-grey">';
            content += 'In Maintenance';
            content += '</p>';
            content += '<span class="status--text-number">';
            content += '63';
            content += '</span>';
            content += '</div>';
            content += '</div>'; */
            $('.status--wrapper_two').append(content);
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in status");
    }
});