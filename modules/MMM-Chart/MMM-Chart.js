/* global Module */

/* Magic Mirror
 * Module: MMM-Chart
 *
 * By Evghenii Marinescu https://github.com/MarinescuEvghenii/
 * MIT Licensed.
 */

Module.register("MMM-Chart", {
    defaults: {
        width       : 640,
        height      : 330,
        chartConfig: {
            type: 'bar',
            data: {
                labels: ["Blup", "Dezember", "Januar", "Bla", "März", "April"],
                datasets: [{
                    label: 'Versandstatistik',
                    data: [37000, 22000, 24000, 25000, 20000, 22500],
                    backgroundColor: [
                        'rgba(112, 195, 237)',
                        'rgba(112, 195, 237)',
                        'rgba(112, 195, 237)',
                        'rgba(112, 195, 237)',
                        'rgba(112, 195, 237)',
                        'rgba(112, 195, 237)',
                    ]
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        },
    },

    getScripts: function() {
		return ["modules/" + this.name + "/node_modules/chart.js/dist/Chart.bundle.min.js"];
	},

	start: function() {
        this.config = Object.assign({}, this.defaults, this.config);
		Log.info("Starting module: " + this.name);
	},

	getDom: function() {
        // Create wrapper element
        console.log("*****");
        //console.log(JSON.stringify(this.config.chartConfig));
        const wrapperEl = document.createElement("div");
        var e =document.createElement("div");    // add div to give size to chart area
        //e.setAttribute("style", "position: relative; display: inline-block;");  // position already set
        e.style.width = this.config.width+"px";   // set right style attribute with size in pixels
        e.style.height = this.config.height+"px";   // set right style attribute with size in pixels     
        wrapperEl.appendChild(e);
        // Create chart canvas
        const chartEl = document.createElement("canvas");
        // Append chart
        e.appendChild(chartEl);
        // Init chart.js
        this.chart = new Chart(chartEl, this.config.chartConfig);

		return wrapperEl;
    },
    
    notificationReceived: function(notification, payload) {
        if(notification === 'CHART') {
            console.log("in mmm chart");
            var theData = [];
            var labels = []
            /* this.chart.data.datasets[0].pop();
            this.chart.data.labels.pop();
            this.chart.update(); */

            var dates = payload.message.value;
            for (let index = 0; index < dates.length; index++) {
                const date = dates[index].Count;
                const label = dates[index].MonatName;
                //console.log(date);
                theData.push(date);
                labels.push(label);
            }
             
            //console.log(this.chart);
            console.log(theData);
            console.log(labels);
            
            this.chart.data.datasets[0].data = theData;
            this.chart.data.labels = labels;
            this.chart.update();
            //console.log(payload.message);
        }
    },

    socketNotificationReceived: function(notification, payload) {
        console.log("in mmm chart socket");
        if(notification == 'CHART') {
            console.log("IN MMM CHART!!!");
        }
    },

});

function updateConfigByMutating(chart) {
    chart.options.title.text = 'new title';
    chart.update();
}