const NodeHelper = require('node_helper');



module.exports = NodeHelper.create({


    socketNotificationReceived: function(notification, payload) {
        console.log("in socket chart node helper");
        console.log(JSON.stringify(payload));
        this.sendSocketNotification('MSG', 'Hello from node helper in mmm chart');
        var self = this;
    },

    notificationReceived: function(notification, payload) {
        console.log("in mmm chart ohne socket");
        console.log(notification);
    }
});