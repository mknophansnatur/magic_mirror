Module.register("status_six",{
	// Default module config.
	defaults: {
		text: "Status"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'status--wrapper status--wrapper_six');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="status--block-twothird">';
            content += '<h4 class="status--headline-4">Raumtemperatur</br> EK</h4>';
            content += '<div class="status--block-helper">';
            content += '<img class="status--block-graph" src="/modules/status/public/fleet-status_new.svg">';
            content += '<img class="status--block-indicator ek" src="/modules/status/public/fleet-status_2_new.svg">';
            /* content += '<div class="status--block-left">';
            content += '</div>'; */
            content += '<div class="status--block-center">';
            content += '<p class="status--block-efficiency ek">';
            content += '00°C';
            content += '</p>';
            content += '<p class="status--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';

            content += '<div class="status--block-twothird">';
            content += '<h4 class="status--headline-4">Raumtemperatur</br>MH</h4>';
            content += '<div class="status--block-helper">';
            content += '<img class="status--block-graph" src="/modules/status/public/fleet-status_new.svg">';
            content += '<img class="status--block-indicator mh" src="/modules/status/public/fleet-status_2_new.svg">';
            /* content += '<div class="status--block-left">';
            content += '</div>'; */
            content += '<div class="status--block-center">';
            content += '<p class="status--block-efficiency mh">';
            content += '00°C';
            content += '</p>';
            content += '<p class="status--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            $('.status--wrapper_six').append(content);
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in status");
    }
});