Module.register("status_five",{
	// Default module config.
	defaults: {
        text: "Status",
        name: "Raumtemperatur AA",
        name2: "Raumtemperatur Flur",
        class_name1: "aa",
        class_name2: "flur",
        wrapper_name: "wrapper_name"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        console.log("in getDOM");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'status--wrapper status--'+this.config.wrapper_name);

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="status--block-twothird '+this.config.class_name1+'">';
            content += '<h4 class="status--headline-4">'+this.config.name+'</h4>';
            content += '<div class="status--block-helper">';
            content += '<img class="status--block-graph" src="/modules/status/public/fleet-status_new.svg">';
            content += '<img class="status--block-indicator '+this.config.class_name1+'" src="/modules/status/public/fleet-status_2_new.svg">';
            /* content += '<div class="status--block-left">';
            content += '</div>'; */
            content += '<div class="status--block-center">';
            content += '<p class="status--block-efficiency '+this.config.class_name1+'">';
            content += '00°C';
            content += '</p>';
            content += '<p class="status--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';

            content += '<div class="status--block-twothird '+this.config.class_name2+'">';
            content += '<h4 class="status--headline-4">'+this.config.name2+'</h4>';
            content += '<div class="status--block-helper">';
            content += '<img class="status--block-graph" src="/modules/status/public/fleet-status_new.svg">';
            content += '<img class="status--block-indicator '+this.config.class_name2+'" src="/modules/status/public/fleet-status_2_new.svg">';
            /* content += '<div class="status--block-left">';
            content += '</div>'; */
            content += '<div class="status--block-center">';
            content += '<p class="status--block-efficiency '+this.config.class_name2+'">';
            content += '00°C';
            content += '</p>';
            content += '<p class="status--block-effiText">';
            content += '';
            content += '</p>';
            content += '</div>';
            content += '</div>';
            content += '</div>';
            
            $('.status--'+this.config.wrapper_name).append(content);
        }
    },


    updateDom: function() {
        console.log("in update dom");

        return true;
    },
    
    start: function() {
        console.log("in status");
    }
});