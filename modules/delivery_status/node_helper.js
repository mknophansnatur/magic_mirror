const NodeHelper = require('node_helper');
const https = require('https');
var server = 'amqp://dashboardit:1Y0J92jgZo5RCVNf@192.168.14.151/monitoring';
//var server = 'amqp://paypalrefund:7q0B96%5DTQ-Uro%40%7DF@192.168.14.151/monitoring'
var amqp = require('amqplib/callback_api');
var exec = require('child_process').exec;
const essen = "https://api.kathke.eu/dine/suederbrarup/today";

var queueName = "ha-nodered-temp";
//var queueName = "ha-dashboard-it-1";
//var queueName = "ha-dashboard-it-2";
var ipAddr = "0";
//var queueName = "ha-dashboard-it-3";
//var queueName = "ha-dashboard-it-4";
if('wlan0' in require('os').networkInterfaces()) {
  var ipAddr = require('os').networkInterfaces().wlan0[0].address;
}
else {
  console.log("nöööö");
}

switch (ipAddr) {
  case "192.168.110.75":
    queueName = "ha-dashboard-it-1";
    break;
  case "192.168.110.78":
    queueName = "ha-dashboard-it-2";
    break;
  case "192.168.110.76":
    queueName = "ha-dashboard-it-3";
    break;
  case "192.168.110.77":
    queueName = "ha-dashboard-it-4";
  break;
  default:
    queueName = "ha-nodered-temp";
    break;
}






module.exports = NodeHelper.create({
    start: function() {
        console.log(" helper started... " + this.name);
        this.sendSocketNotification('MSG', 'Hello World');
        var self = this;


        function start() {
            amqp.connect(server + "?heartbeat=60", function(err, conn) {
              if (err) {
                console.error("[AMQP]", err.message);
                return setTimeout(start, 1000);
              }
              conn.on("error", function(err) {
                if (err.message !== "Connection closing") {
                  console.error("[AMQP] conn error", err.message);
                }
              });
              conn.on("close", function() {
                console.error("[AMQP] reconnecting");
                return setTimeout(start, 1000);
              });
          
              console.log("[AMQP] connected");
              amqpConn = conn;
          
              whenConnected();
            });
          }
          
          function whenConnected() {
            startPublisher();
            startWorker();
          }
          
          var pubChannel = null;
          var offlinePubQueue = [];
          function startPublisher() {
            amqpConn.createConfirmChannel(function(err, ch) {
              if (closeOnErr(err)) return;
              ch.on("error", function(err) {
                console.error("[AMQP] channel error", err.message);
              });
              ch.on("close", function() {
                console.log("[AMQP] channel closed");
              });
          
              pubChannel = ch;
              while (true) {
                var m = offlinePubQueue.shift();
                if (!m) break;
                publish(m[0], m[1], m[2]);
              }
            }); 
          }
          
          // method to publish a message, will queue messages internally if the connection is down and resend later
          function publish(exchange, routingKey, content) {
            try {
                pubChannel.publish(exchange, routingKey, content, { persistent: true },
                function(err, ok) {
                if (err) {
                    console.error("[AMQP] publish", err);
                    offlinePubQueue.push([exchange, routingKey, content]);
                    pubChannel.connection.close();
                }
                });
            } catch (e) {
              console.error("[AMQP] publish", e.message);
              offlinePubQueue.push([exchange, routingKey, content]);
            }
          }
          
          // A worker that acks messages only if processed succesfully
          function startWorker() {
            amqpConn.createChannel(function(err, ch) {
              if (closeOnErr(err)) return;
              ch.on("error", function(err) {
                console.error("[AMQP] channel error", err.message);
              });
              ch.on("close", function() {
                console.log("[AMQP] channel closed");
              });
              ch.prefetch(10);
              ch.assertQueue(queueName, { durable: true }, function(err, _ok) {
                if (closeOnErr(err)) return;
                ch.consume(queueName, processMsg, { noAck: true });
                console.log("Worker is started");
              });
          
              function processMsg(msg) {
                var path = '/opt/magic_mirror/modules/testmodule/message.txt';
                work(msg, function(ok) {
                  try {
                    if (ok) {
                      try {
                        console.log(msg.content.toString());
                      } catch(err) {
                        console.error(err);
                      }
                      self.sendSocketNotification('RBMQ', msg.content.toString());
                      /* https.get(essen, (resp) => {
                        let data = '';
          
                        resp.on('data', (chunk) => {
                          data += chunk;
                          console.log(data);
                          self.sendSocketNotification('ESSEN', data.toString());                      
                        })
                      }); */
                        //ch.ack(msg);
                    }
                    else
                      ch.reject(msg, true);
                  } catch (e) {
                    closeOnErr(e);
                  }
                });
              }
            });
          }
          
          function work(msg, cb) {
            console.log("Got msg", msg.content.toString());
            cb(true);
          }
          
          function closeOnErr(err) {
            if (!err) return false;
            console.error("[AMQP] error", err);
            amqpConn.close();
            return true;
          }
          
          setInterval(function() {
            publish("", queueName, new Buffer("buffering"));
          }, 10000);
          
          start();

    },

    socketNotificationReceived: function(notification, payload) {
        console.log("received msg new");
        console.log(JSON.stringify(payload));
        console.log(JSON.stringify(notification));
        this.sendSocketNotification('MSG', 'Hello from delivery status');
        if(notification === 'SCREEN') {
          console.log(payload.message);
          console.log();
          var command = payload.message.value;
          exec(command, function callback(error, stdout, stderr){
            console.log(stdout);
            console.log(error);
            console.log(stderr);
          });
          console.log("notifaction is " + notification);
          this.sendSocketNotification('SCREEN', 'Hello from delivery status screen geht jetzt aus!!!');
        }
        if(notification === 'iptest') {
          console.log("in nodehelper:");
          console.log(queueName);
          //console.log(require('os').networkInterfaces().wlan0[0].address);
        }
        var self = this;
    }

});