Module.register("delivery_status",{
	// Default module config.
	defaults: {
		text: "Delivery Status"
    },
    
    getScripts: function() {
		return [
            this.file("../testmodule/node_modules/jquery/dist/jquery.js"),
            this.file("../MMM-Chart/node_modules/chart.js/dist/Chart.bundle.min.js"),
            this.file("public/progress_circle.js")
        ];
    },

    loaded: function() {
        console.log("in loaded");
    },

	// Override dom generator.
	getDom: function() {
        //console.log("in getDOM ");
        var wrapper = document.createElement("div");
        wrapper.setAttribute('class', 'delivery_status--wrapper');

		return wrapper;
    },

    notificationReceived: function(notification, payload, sender) {
        if(notification == 'DOM_OBJECTS_CREATED') {
            var content;
            content = '';
            content += '<div class="delivery_status--container">';
            content += '<h4 class="delivery_status--headline">'+'Delivery Status'+'</h4>';

            content += '<div class="delivery_status--graph-wrapper">';
            content += '<div class="delivery_status--graph-container">';
            content += '<svg class="progress-ring" height="240" width="240"> <circle class="progress-ring__circle" stroke-width="20" fill="transparent" stroke="orange"  r="110" cx="120" cy="120"/></svg>';
            content += '<div class="graph--text-wrapper">';
            content += '<p class="delivery_status--graph-number">';
            content += '99%';
            content += '</p>';
            content += '<p class="delivery_status--graph-text">';
            content += 'Within Time Limit';
            content += '</p>';
            //graph text wrapper close
            content += '</div>';
            //graph container close
            content += '</div>';

            content += '<div class="delivery_status--text-wrapper">';
            content += '<div class="delivery_status--text-row">';
            content += '<p class="delivery_status--text-copytext">';
            content += 'Within Time Limit';
            content += '</p>';
            content += '<p class="delivery_status--text-number">';
            content += '502';
            content += '</p>';
            //text row close
            content += '</div>';

            
            content += '<div class="delivery_status--text-row">';
            content += '<p class="delivery_status--text-copytext">';
            content += 'Out of Time Limit';
            content += '</p>';
            content += '<p class="delivery_status--text-number">';
            content += '6';
            content += '</p>';
            //text row close
            content += '</div>';

            //text wrapper close
            content += '</div>';

            //graph wrapper close
            content += '</div>';
            //wrapper close
            content += '</div>';
            //$('.delivery_status--wrapper').append(content);
        }
        else{
            //console.log(notification);
            //console.log(payload);
        }
    },


    updateDom: function() {
        //console.log("in update dom");

        return true;
    },

    socketNotificationReceived: function(notification, payload) {
        var self = this;
        //console.log("**************************** in socket notification")
        //console.log(notification);
        //console.log(payload);
        console.log(notification);
        if(notification == 'RBMQ') {
            //console.log("type =   " + typeof payload);
            //console.log("in delivery status socketNotification Received");
            //console.log(payload);
            if(isJson(payload)) {
                var test = JSON.parse(payload);
                console.log(test);
                if(test.hasOwnProperty("type")) {
                    if(test.type === 'command') {
                        console.log("BILDSCHIRM AUS!");
                        this.sendSocketNotification('SCREEN', {message: test});
                    }
                    if(test.type === 'iptest') {
                        console.log("IN IP TEST YOOOOOOOOOOOOOOOO");
                        console.log(location.hostname);
                        this.sendSocketNotification('iptest', {message: test});
                        try {
                            var network = new ActiveXObject('WScript.Network');
                            // Show a pop up if it works
                            console.log("computer name:");
                            console.log(network.computerName);
                        }
                        catch (e) { }
                    }
                    if(test.type === 'deliveryStatisticsPerMonth') {
                        self.sendNotification('CHART', {message: test});
                    }
                    if(test.type === "temperatur") {
                        translateTemp(payload);
                    }
                    if(test.type === "deliveryStatistics") {
                        self.sendNotification('PIE', {message: test});
                    }
                    if(test.type === "deliveryStatisticsToday") {
                        self.sendNotification('LABELS', {message: test});
                    }
                    if(test.type === "labelsPerMinute") {
                        self.sendNotification('MINUTE', {message: test});
                    }
                    if(test.type === "deliveryStatisticsPerHourFromToday" || test.type === "deliveryStatisticsPerHourFromLastWeek") {
                        self.sendNotification('COMPARE', {message: test});
                    }
                    if(test.type === "deliveryStatisticsPerHourFromTodayHistory") {
                        self.sendNotification('LINEGRAPH', {message: test});
                    }
                    if(test.type === "AktuelleErzeugung") {
                        self.sendNotification('VORLAUF', {message: test});
                    }
                    if(test.type === "TemperaturServerraum") {
                        translateTemp(payload);
                    }
                    if(test.type === "TemperaturDraußen") {
                        translateTemp(payload);
                    }
                    if(test.type === "TemperaturHAR") {
                        translateTemp(payload);
                    }
                    if(test.type === "TemperaturHalle2") {
                        translateTemp(payload);
                    }
                    if(test.type === "PV") {
                        translatePV(payload);
                    }
                    if(test.type === "Alarm") {
                       handleThisShit(test);
                    }
                }
            }
            
        }
        if(notification == 'ESSEN') {
            var test = JSON.parse(payload);
            self.sendNotification('FOOD', {message: test});
            //console.log(test);
        }
    },
    
    start: function() {
        var self = this;
        this.sendSocketNotification('START', {message: 'start connection'});
        //console.log("in delivery_status");
    }
});

function handleThisShit(payload) {
    //console.log("From nodehelper, works yuhu");
    if(payload) {
        //console.log(notification);
        //console.log(payload);
        self.sendNotification("PAGE_CHANGED", 3);
        self.sendNotification("PAUSE_ROTATION");
        self.sendNotification("ERROR", {message: "ALARM IN " + payload.name});
        $('.alert-window').addClass("fire");
        $('.fa.fa-circle.indicator').removeClass("resume");
    }
    
    
}

function translatePV(payload) {
    var value = JSON.parse(payload);
    var key = value.name;
    var value = value.value;
    switch(key) {
        case 'PV aktuelle Erzeugung':
            $(".pv--block-efficiency.rightnow").text(value);
        break;
        case 'PV Tagesertrag':
            value = value.match(/\d+/);
            if(value > 9999) {
                value = value / 1000;
                value = Math.round(value);
            }
            $(".pv--block-efficiency.dailyuse").text(value+" kWh");
        break;
        case 'PV UDC':
            $(".pv--block-efficiency.udc").text(value);
        break;
        case 'PV gestriger Ertrag':
            value = value.match(/\d+/);
            if(value > 9999) {
                value = value / 1000;
                value = Math.round(value);
            }
            $(".pv--block-efficiency.yesterday").text(value+" kWh");
        break;
        case 'PV Monat':
            value = value.match(/\d+/);
            if(value > 9999) {
                value = value / 1000;
                value = Math.round(value);
            }
            $(".pv--block-efficiency.month").text(value + " kWh");
        break;
        case 'PV Jahr':
            value = value.match(/\d+/);
            if(value > 9999) {
                value = value / 1000;
                value = Math.round(value);
            }
            $(".pv--block-efficiency.year").text(value+" kWh");
        break;
        case 'PV Momentan PAC':
            $(".pv--block-efficiency.pacnow").text(value);
        break;
        case 'PV Gesamt':
            value = value.match(/\d+/);
            if(value > 9999) {
                value = value / 1000;
                value = Math.round(value);
            }
            $(".pv--block-efficiency.all").text(value+" kWh");
        break;
        case 'PV Installierte Generatorleistung':
            value = value.match(/\d+/);
            if(value > 9999) {
                value = value / 1000;
                value = Math.round(value);
            }
            $(".pv--block-efficiency.installed").text(value+" kWp");
        break;
        case 'PV last update':
            $(".pv--block-efficiency.update").text(value);
        break;
        default:
            break;
    }
}

function translateTemp(payload) {
    //console.log("in translate");
    var test = JSON.parse(payload);
    //console.log(typeof test);
    //console.log(test.name)
    var key = test.name;

    switch (key) {
        case 'Raumtemperatur IT':
            //console.log('in switch case');
            //$('.status--block-indicator').addClass("test");
            $('.status--block-indicator.it').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.it').text(test.value+"°C");
            /* data = [8, 8, 5, 20, 5, 8];
            addData("canvas", "# of Votes", data); */
            break;
        case 'Raumtemperatur WC Herren':
            $('.status--block-indicator.wc').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.wc').text(test.value+"°C");
            break;
        case 'Raumtemperatur Pausenraum':
            $('.status--block-indicator.break').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.break').text(test.value+"°C");
            break;
        case 'Raumtemperatur Buchhaltung':
            $('.status--block-indicator.bh').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.bh').text(test.value+"°C");
            break;
        case ('Raumtemperatur WC Damen'):
            $('.status--block-indicator.wcd').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.wcd').text(test.value+"°C");
            break;
        case ('Raumtemperatur Meeting'):
            $('.status--block-indicator.meeting').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.meeting').text(test.value+"°C");
            break;
        case 'Raumtemperatur Schneiderei':
            $('.status--block-indicator.sc').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.sc').text(test.value+"°C");
            break;
        case 'Raumtemperatur Laden':
            $('.status--block-indicator.shop').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.shop').text(test.value+"°C");
            break;
        case ('Raumtemperatur AA'):
            $('.status--block-indicator.aa').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.aa').text(test.value+"°C");
            break;
        case ('Raumtemperatur Flur'):
            $('.status--block-indicator.flur').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.flur').text(test.value+"°C");
            break;
        case 'Raumtemperatur EK':
            $('.status--block-indicator.ek').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.ek').text(test.value+"°C");
            break;
        case 'Raumtemperatur MH':
            $('.status--block-indicator.mh').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.mh').text(test.value+"°C");
            break;
        case 'Temperatur Serverraum':
            $('.status--block-indicator.server').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.server').text(test.value+"°C");
            break;
        case 'Temperatur Draußen':
            $('.status--block-indicator.draußen').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.draußen').text(test.value+"°C");
            break;
        case 'Temperatur Halle 2':
            $('.status--block-indicator.halle2').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.halle2').text(test.value+"°C");
            break;
        case 'Temperatur HAR':
            $('.status--block-indicator.HAR').css('transform', 'rotate('+(test.value*9)+'deg)');
            $('.status--block-efficiency.HAR').text(test.value+"°C");
            break;
        default:
            break;
    }
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}