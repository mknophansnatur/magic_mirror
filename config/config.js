/* Magic Mirror Config Sample
 *
 * By Michael Teeuw http://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information how you can configurate this file
 * See https://github.com/MichMich/MagicMirror#configuration
 *
 */

var config = {
  address: "0.0.0.0", // Address to listen on, can be:
  // - "localhost", "127.0.0.1", "::1" to listen on loopback interface
  // - another specific IPv4/6 to listen on a specific interface
  // - "0.0.0.0" to listen on any interface
  // Default, when address config is left out, is "localhost"
  port: 8080,
  ipWhitelist: [], // Set [] to allow all IP addresses
  // or add a specific IPv4 of 192.168.1.5 :
  // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
  // or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
  // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

  language: "de",
  timeFormat: 24,
  units: "metric",

  modules: [
    {
      module: 'MMM-pages',
      rotationTime: 3000,
      delayTime: 2000,
      animationTime: 1000,
      config: {
        modules:
            [/* ["profit", "status", "delivery_status", "MMM-Chart", "compare", "average", "profit_graph", "status_two"],
            ["profit","status_three", "status_four", "status_five", "status_six"],
            ["profit","pv", "pv_double", "vorlauf"],*/
              ["MMM-Trello"],
              ["MMM-cryptocurrency"],
              ["newsfeed", "MMM-Fuel", "MMM-OpenmapWeather"],
              /* ["MMM-iFrame"] */,
              //["new_relic_alert", "error_handler"]
            ],
        fixed: [ "MMM-page-indicator"]
      }
    },
    {
      module: 'MMM-page-indicator',
      position: 'bottom_bar',
      config: {
        pages: 3
      }
    },
    /*{
      module: "testmodule",
      header: "Hans Natur Kalender",
      position: "top_right"
    },*/
    {
      module: "profit",
      header: "Versand in Länder",
      position: "top_left"
    },
    {
      module: "status",
      position: "top_center"
    },
    {
      module: "pv",
      position: "top_center",
      config: {
        wrapper_name: "wrapper_pv_1",
        name: "Aktuelle Erzeugung",
        class_name1: "rightnow"
      }
    },
    {
      module: "pv",
      position: "top_center",
      config: {
        wrapper_name: "wrapper_pv_2",
        name: "PV Tagesertrag",
        class_name1: "dailyuse"
      }
    },
    {
      module: "pv",
      position: "top_center",
      config: {
        wrapper_name: "wrapper_pv_3",
        name: "PV UDC",
        class_name1: "udc"
      }
    },
    {
      module: "pv",
      position: "top_center",
      config: {
        wrapper_name: "wrapper_pv_4",
        name: "PV Monat",
        class_name1: "month"
      }
    },
    {
      module: "pv_double",
      position: "top_right",
      config: {
        wrapper_name: "wrapper_pv_5",
        name1: "PV gestriger Ertrag",
        class_name1: "yesterday",
        name2: "PV Gesamt",
        class_name2: "all"
      }
    },
    {
      module: "pv",
      position: "top_center",
      config: {
        wrapper_name: "wrapper_pv_6",
        name: "PV Jahr",
        class_name1: "year"
      }
    },
    {
      module: "pv",
      position: "top_center",
      config: {
        wrapper_name: "wrapper_pv_7",
        name: "PV Momentan PAC",
        class_name1: "pacnow"
      }
    },
    {
      module: "pv",
      position: "top_right",
      config: {
        wrapper_name: "wrapper_pv_8",
        name: "PV Installierte Generatorleistung",
        class_name1: "installed"
      }
    },
    {
      module: "pv",
      position: "top_right",
      config: {
        wrapper_name: "wrapper_pv_9",
        name: "PV last update",
        class_name1: "update"
      }
    },
    {
      module: "vorlauf",
      position: "top_center",
    },
    //PV Momentan PAC
    {
      module: "delivery_status",
      position: "top_right"
    },
    {
      module: 'MMM-Chart',
      position: "top_right",
    },
    {
      module: "compare",
      position: "lower_third"
    },
    {
      module: "average",
      position: "upper_third"
    },
    {
      module: "profit_graph",
      position: "bottom_center"
    },
    {
      module: "status_two",
      position: "bottom_right"
    },
    {
      module: "status_three",
      position: "top_center"
    },
    {
      module: "status_four",
      position: "top_right"
    },
    {
      module: "status_five",
      position: "middle_center",
      config: {
        wrapper_name: "wrapper_five",
        name: "Raumtemperatur AA"
      }
    },
    {
      module: "status_five",
      position: "lower_third",
      config: {
        wrapper_name: "wrapper_seven",
        name: "Temperatur Serverraum",
        name2: "Außentemperatur",
        class_name1: "server",
        class_name2: "draußen"
      }
    },
    {
      module: "status_five",
      position: "lower_third",
      config: {
        wrapper_name: "wrapper_eight",
        name: "Temperatur Halle 2",
        name2: "Temperatur HAR",
        class_name1: "halle2",
        class_name2: "HAR"
      }
    },
    {
      module: "status_six",
      position: "upper_third"
    },
    {
      header: "Auftragsannahme",
      module: 'MMM-Trello',
      position: 'top_left', // This can be any of the regions, best results in center regions.
      config: {
        // See 'Configuration options' for more information.
        api_key: "53e1a46490558d0bcbeebfff6d850ded",
        reloadInterval: 60000,
        showDescription: false,
        updateInterval: 2500,
        animationSpeed: 500,
        token: "c4a9e030a9fcaa84d755012828d7e3e5dbb5cbf7e4f1a672c4b3b63143131b93",
        list: "5eb959bbf8858430f923a888",
      }
    },
    {
      header: "Lager",
      module: 'MMM-Trello',
      position: 'top_left', // This can be any of the regions, best results in center regions.
      config: {
        // See 'Configuration options' for more information.
        showDescription: false,
        api_key: "53e1a46490558d0bcbeebfff6d850ded",
        updateInterval: 2500,
        animationSpeed: 500,
        reloadInterval: 60000,
        token: "c4a9e030a9fcaa84d755012828d7e3e5dbb5cbf7e4f1a672c4b3b63143131b93",
        list: "5eb95997d758a17add692ebd",
      }
    },
    {
      header: "Buchhaltung",
      module: 'MMM-Trello',
      position: 'top_left', // This can be any of the regions, best results in center regions.
      config: {
        showDescription: false,
        // See 'Configuration options' for more information.
        api_key: "53e1a46490558d0bcbeebfff6d850ded",
        updateInterval: 2500,
        animationSpeed: 500,
        reloadInterval: 60000,
        token: "c4a9e030a9fcaa84d755012828d7e3e5dbb5cbf7e4f1a672c4b3b63143131b93",
        list: "5eb9599a8e61e119c223103b",
      }
    },
    //5eb9599a8e61e119c223103b
    {
      header: "Webshop",
      module: 'MMM-Trello',
      position: 'top_right', // This can be any of the regions, best results in center regions.
      config: {
        showDescription: false,
        // See 'Configuration options' for more information.
        api_key: "53e1a46490558d0bcbeebfff6d850ded",
        updateInterval: 2500,
        animationSpeed: 500,
        reloadInterval: 60000,
        token: "c4a9e030a9fcaa84d755012828d7e3e5dbb5cbf7e4f1a672c4b3b63143131b93",
        list: "5ebc0853a1499f20e4b92ac3",
      }
    },
    {
      header: "Großraumbüro",
      module: 'MMM-Trello',
      position: 'top_right', // This can be any of the regions, best results in center regions.
      config: {
        // See 'Configuration options' for more information.
        api_key: "53e1a46490558d0bcbeebfff6d850ded",
        showDescription: false,
        updateInterval: 2500,
        reloadInterval: 60000,
        animationSpeed: 500,
        token: "c4a9e030a9fcaa84d755012828d7e3e5dbb5cbf7e4f1a672c4b3b63143131b93",
        list: "5eb959a1e991386311dbb3ca",
      }
    },
    {
      header: "Sonstiges",
      module: 'MMM-Trello',
      position: 'top_right', // This can be any of the regions, best results in center regions.
      config: {
        // See 'Configuration options' for more information.
        api_key: "53e1a46490558d0bcbeebfff6d850ded",
        showDescription: false,
        updateInterval: 2500,
        reloadInterval: 60000,
        animationSpeed: 500,
        token: "c4a9e030a9fcaa84d755012828d7e3e5dbb5cbf7e4f1a672c4b3b63143131b93",
        list: "5eb959aa5859921d9ae170da",
      }
    },
    {
      module: "newsfeed",
      position: "top_left",	// This can be any of the regions. Best results in center regions.
      config: {
        // The config property is optional.
        // If no config is set, an example calendar is shown.
        // See 'Configuration options' for more information.

        feeds: [
          {
            title: "DEUTSCHLAND",
            url:"https://rss.dw.com/xml/rss-de-news"
          },
          /*{
            title: "SPORT",
            url:"http://www.ndr.de/sport/index-rss.xml"
          }*/
        ]
      }
    },
    /*{
      module: 'essen',
      position: 'middle_center'
    },*/
    {
      module: 'MMM-cryptocurrency',
      position: 'fullscreen_below',
      config: {
        //apikey: '4be358d9-86a2-4462-b93c-80d990e88243',
        apikey: 'eab1b2d8-7f19-46a0-b08a-5e9ca1f0c8c5',
        currency: ['bitcoin', 'ethereum', 'xrp', 'cardano', 'stellar', 'iota', 'bitpanda-ecosystem-token', 'pantos'],
        conversion: 'EUR',
        headers: ['change1h', 'change24h', 'change7d'],
        displayType: 'logoWithChanges',
        showGraphs: true,
        displayLongNames: true
      }
    },
    {
      module: "MMM-Fuel",
      position: "bottom_bar",
      config: {
        api_key: "571a40a5-9d40-47ee-f72d-c567f6d9e06a",
        lat: 54.642117,
        lng: 9.761811,
        types: ["e5", "diesel"],
        sortBy: "e5"
        // all your config options, which are different than their default values
      }
    },
    {
      module: "MMM-OpenmapWeather",
      position: "top_right",	// This can be any of the regions.
      // Best results in left or right regions.
      config: {
        // See 'Configuration options' for more information.
        location: "Süderbrarup,DE",
        degreeLabel: true,
        locationID: "", //Location ID from http://openweathermap.org/help/city_list.txt
        appid: "c9659c6d9f319a77a6078c27e24f179f",  //openweathermap.org API key
        colorIcon: true
      }
    },
    {
      module: 'MMM-iFrame',
      position: 'top_left',	// This can be any of the regions.
      config: {
        // See 'Configuration options' for more information.
        url: ["https://chart-embed.service.newrelic.com/herald/f59a70de-d748-4fa0-8695-e7ee80884ecc?height=1800px"],  // as many URLs you want or you can just ["ENTER IN URL"] if single URL.
        updateInterval: 0.5 * 60 * 1000, // rotate URLs every 30 seconds
        width: "1920", // width of iframe
        height: "1080", // height of iframe
        frameWidth: "1000" // width of embedded iframe, height is beeing calculated by aspect ratio of iframe
      }
    },
    {
      module: 'MMM-iFrame',
      position: 'top_center',
      config: {
        url: ["https://chart-embed.service.newrelic.com/herald/b45f6a1f-ea6b-4635-bcc3-f012ed5f5c1e?height=1422px"],
        updateInterval: 0.5 * 60 * 1000,
        width: "1920",
        height: "1080",
        frameWidth: "800"
      }
    },
    {
      module: 'MMM-iFrame',
      position: 'lower_third',
      config: {
        url: ["https://chart-embed.service.newrelic.com/herald/8d11cfc6-8368-4408-8bd8-c5d0d404ebdc"],
        //url: ["https://chart-embed.service.newrelic.com/herald/c19e8268-1a28-4ff9-bc04-0d32307cd16e"],
        //url: ["https://rpm.newrelic.com/public/charts/iU1YYttfoL7"],
        updateInterval: 0.5 * 60 * 1000,
        width: "1920",
        height: "1080",
        frameWidth: "1000"
      }
    },
    {
      module: 'new_relic_alert',
      position: 'bottom_center'
    },
    {
      module: 'error_handler',
      position: 'top_center'
    }
    //5eb959aa5859921d9ae170da
    //5eb959a1e991386311dbb3ca

    /* {
      module: "avg_delivery_time",
      position: "lower_third"
    }, */

  ]

};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") { module.exports = config; }
